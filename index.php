 <?php 

//Redirects users depending on delivery date to customer

$_GET += array('reqID' => null, 'reqDate' => null); // define

$reqID = $_GET['reqID'];
$reqDate = $_GET['reqDate'];

$reqIDSec = filter_var($reqID, FILTER_SANITIZE_STRING);
$reqDateSec = filter_var($reqDate, FILTER_SANITIZE_STRING);

$surveyURL = 'https://www.surveymonkey.co.uk/r/87JBLTP?reqId='.$reqIDSec;

if(strtotime($reqDateSec) < strtotime('-14 days')) {
     // this is true
	header( 'Location: https://www.millar-bryce.com/survey-expired/' ) ;
	 } else {
 	header( 'Location: ' .$surveyURL ) ;
 }

?> 